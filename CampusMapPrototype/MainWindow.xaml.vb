﻿Imports System.Windows.Media.Animation
Imports System.Windows.Threading
Imports System.Data
Imports System.Reflection
Imports System.Text.RegularExpressions



Class MainWindow
    'master scale
    Dim scale As New ScaleTransform
    'rate of zoom
    Dim scalerate As Double = 1.4
    Dim zoomlevel As Double = 1.0
    Dim canvH As Double
    Dim canvW As Double
    Dim ClickCount As Integer
    Dim Directions As Boolean = False


    Structure Building
        Dim buildingName As String
        Dim Zoomlevel As Double
        Dim floors As List(Of String)
        Dim x As Integer
        Dim y As Integer
        Dim z As Integer
    End Structure

    Dim searchSelection As String = ""
    Dim selectedBuilding As String = ""
    Dim prevBuilding As String = "MaverickCenter"

    Dim buildingNames() As String = {"Albers Hall", "Admissions Welcome Center", "Bunting Hall", "Development Center", "Dominguez Hall", "Escalante Hall", "Fine Arts Building", "Garfield Hall", "Grand Mesa Hall", "Health Sciences North", "Houston Hall", "Lowell Heiny Hall", "Maverick Center", "Maverick Pavilion", "Monument Hall", "Moss Performing Arts Center", "North Avenue Hall", "Orchard Avenue Apartments", "Outdoor Program Building", "Pinon Hall", "Rait Hall", "Tolman Hall", "Tomlinson Library", "University Center", "Walnut Ridge Apartments", "Wubben Math and Science Center"}
    Dim buildings(25) As Building

    Dim p As Point = Mouse.GetPosition(ScrlViewMap)
    Dim mouseisDown As Boolean = False
    Dim xchange As Integer = 0
    Dim ychange As Integer = 0

    'executed immediately after window has loaded
    Private Sub MainWindow1_Loaded(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Me.WindowState = Windows.WindowState.Maximized
        canvW = campusmap.ActualWidth
        canvH = campusmap.ActualHeight
        scale.ScaleX = 1.0
        scale.ScaleY = 1.0
        scale.CenterX = campusmap.ActualWidth / 2
        scale.CenterY = campusmap.ActualHeight / 2
        campusmap.RenderTransform = scale
        buildingCombobox.Items.Clear()
        DirStartCombobox.Items.Clear()
        DirEndCombobox.Items.Clear()
        Dim counter As Integer = 0
        For Each name As String In buildingNames
            buildings(counter).buildingName = buildingNames(counter)

            buildingCombobox.Items.Add(buildingNames(counter))
            DirStartCombobox.Items.Add(buildingNames(counter))
            DirEndCombobox.Items.Add(buildingNames(counter))
            counter += 1
        Next

        Me.Show()
        searchBox.Focus()


    End Sub

    Private Sub Building_MouseEnter(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Try
            Dim thisStoryBoard As Storyboard = DirectCast(FindResource("BuildingMouseEnter"), Storyboard)
            thisStoryBoard.SetValue(Storyboard.TargetNameProperty, TryCast(sender, Path).Name)
            thisStoryBoard.Begin()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub PathReveal(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Try
            Dim thisStoryBoard As Storyboard = DirectCast(FindResource("ShowPathSegment"), Storyboard)
            thisStoryBoard.SetValue(Storyboard.TargetNameProperty, TryCast(sender, Path).Name)
            thisStoryBoard.Begin()
        Catch ex As Exception
            MsgBox("PathReveal: " + ex.Message)
        End Try
    End Sub
    Private Sub PathHide(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Try
            Dim thisStoryBoard As Storyboard = DirectCast(FindResource("HidePathSegment"), Storyboard)
            thisStoryBoard.SetValue(Storyboard.TargetNameProperty, TryCast(sender, Path).Name)
            thisStoryBoard.Begin()
        Catch ex As Exception
            MsgBox("PathHide: " + ex.Message)
        End Try
    End Sub

    Private Sub Building_MouseClick(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles ScrlViewMap.PreviewMouseLeftButtonDown
        If Directions = False Then
            If viewCampusButton.IsEnabled = False Then
                Try
                    Dim thisStoryBoard As Storyboard = DirectCast(FindResource("BuildingMouseEnter"), Storyboard)
                    selectedBuilding = (thisStoryBoard.GetValue(Storyboard.TargetNameProperty))
                    If selectedBuilding = "WubbenMathandScienceCenter" Then
                        buildingCombobox.Text = "Wubben Math and Science Center"
                    ElseIf selectedBuilding = "TomlinsonOld" Then
                        buildingCombobox.Text = "Tomlinson Library"
                    Else
                        selectedBuilding = Regex.Replace(selectedBuilding, "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1 ")
                        buildingCombobox.Text = selectedBuilding
                    End If
                    'thisStoryBoard.Begin()
                Catch ex As Exception
                    MsgBox("Building_MouseClick: " + ex.Message)
                End Try
            End If
        End If
    End Sub

    Private Sub Building_MouseLeave(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Try
            Dim thisStoryBoard As Storyboard = DirectCast(FindResource("BuildingMouseLeave"), Storyboard)
            thisStoryBoard.SetValue(Storyboard.TargetNameProperty, TryCast(sender, Path).Name)
            thisStoryBoard.Begin()

        Catch ex As Exception
            Console.WriteLine("Building Mouseover: " & ex.Message)
        End Try
    End Sub

    'used by all zoom controls
    Public Sub ZoomIn()
        If zoomlevel <= 10.0 Then
            scale.ScaleX *= scalerate
            scale.ScaleY *= scalerate
            zoomlevel += 0.1
        End If
    End Sub
    Public Sub ZoomOut()
        If zoomlevel >= 1.0 Then
            scale.ScaleX /= scalerate
            scale.ScaleY /= scalerate
            zoomlevel -= 0.1
        End If
    End Sub

    Private Sub BtnZoomIn_Click(sender As Object, e As RoutedEventArgs) Handles BtnZoomIn.Click
        ZoomIn()
    End Sub

    Private Sub BtnZoomOut_Click(sender As Object, e As RoutedEventArgs) Handles BtnZoomOut.Click
        ZoomOut()
    End Sub

    Private Sub getDirections_Click(sender As Object, e As RoutedEventArgs) Handles directionsButton.Click
        Dim sbToolBoxBorder As Storyboard = DirectCast(FindResource("ChangeToolBoxBorder"), Storyboard)
        sbToolBoxBorder.SetValue(Storyboard.TargetNameProperty, "ToolBox")
        Dim sbToolBoxBackground As Storyboard = DirectCast(FindResource("ChangeToolBoxBackground"), Storyboard)
        sbToolBoxBackground.SetValue(Storyboard.TargetNameProperty, "ToolBoxGrid")
        If Directions = False Then
            'DirectionBox.Visibility = Windows.Visibility.Visible
            Directions = True
            directionsButton.Content = "Clear"
            lblSideMenuTitle.Content = "Get Directions"
            lblBuildingbox.Content = "Starting Location"
            lblFloorbox.Content = "Destination"
            lblRoombox.Content = ""
            sbToolBoxBorder.Begin()
            sbToolBoxBackground.Begin()
            'ToolBox.BorderBrush = "#FF7B0537"
            buildingCombobox.Visibility = Windows.Visibility.Hidden
            FloorCombobox.Visibility = Windows.Visibility.Hidden
            RoomCombobox.Visibility = Windows.Visibility.Hidden
            btnShowRoute.Visibility = Windows.Visibility.Visible
            DirStartCombobox.Visibility = Windows.Visibility.Visible
            DirEndCombobox.Visibility = Windows.Visibility.Visible
        Else
            Directions = False
            DirectionBox.Visibility = Windows.Visibility.Hidden
            directionsButton.Content = "Get Directions"
            lblSideMenuTitle.Content = "Building Directory"
            lblBuildingbox.Content = "Building"
            lblFloorbox.Content = "Floor"
            lblRoombox.Content = "Room"
            sbToolBoxBorder.Stop()
            sbToolBoxBackground.Stop()
            buildingCombobox.Visibility = Windows.Visibility.Visible
            FloorCombobox.Visibility = Windows.Visibility.Visible
            'RoomCombobox.Visibility = Windows.Visibility.Visible
            btnShowRoute.Visibility = Windows.Visibility.Hidden
            DirStartCombobox.Visibility = Windows.Visibility.Hidden
            DirEndCombobox.Visibility = Windows.Visibility.Hidden
            Dim currentpaths() As Object = {path01, path02, path03, path04, path05, path06, path07, path08, path09, path10, path11, path12, path13, path14, path15, path16}
            For Each name As Object In currentpaths
                PathHide(name, e)
            Next
        End If

    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Dim wubben As String = "Wubben Math and Science Center"
        Dim houston As String = "Houston Hall"
        Dim maverick As String = "Maverick Center"
        Dim uc As String = "University Center"
        Dim garfield As String = "Garfield Hall"
        Dim startLoc As String = DirStartCombobox.SelectedItem
        Dim endLoc As String = DirEndCombobox.SelectedItem
        If startLoc = wubben And endLoc = maverick Then
            Dim currentpaths() As Object = {path01, path02, path03, path04, path05}
            For Each name As Object In currentpaths
                PathReveal(name, e)
            Next
        ElseIf startLoc = houston And endLoc = uc Then
            Dim currentpaths() As Object = {path10, path11, path12}
            For Each name As Object In currentpaths
                PathReveal(name, e)
            Next
        ElseIf startLoc = houston And endLoc = garfield Then
            Dim currentpaths() As Object = {path10, path13, path14, path15, path16}
            For Each name As Object In currentpaths
                PathReveal(name, e)
            Next
        End If
        DirEndCombobox.SelectedIndex = -1
        DirStartCombobox.SelectedIndex = -1
        btnShowRoute.IsEnabled = False
    End Sub

    Private Sub DirEndCombobox_selectionchanged(ByVal sender As Object, e As RoutedEventArgs) Handles DirEndCombobox.SelectionChanged
        btnShowRoute.IsEnabled = True
    End Sub

    Private Sub ViewCampus_Click(sender As Object, e As RoutedEventArgs) Handles viewCampusButton.Click
        scale.ScaleX = 1.0
        scale.ScaleY = 1.0
        GMH1.Visibility = Windows.Visibility.Hidden
        GMH2.Visibility = Windows.Visibility.Hidden
        GMH3.Visibility = Windows.Visibility.Hidden
        GMH4.Visibility = Windows.Visibility.Hidden
        Wubben1.Visibility = Windows.Visibility.Hidden
        Wubben2.Visibility = Windows.Visibility.Hidden
        Wubben3.Visibility = Windows.Visibility.Hidden
        Parking1.Visibility = Windows.Visibility.Visible
        Misc1.Visibility = Windows.Visibility.Visible
        TextBlock2.Visibility = Windows.Visibility.Visible
        Terrain1.Visibility = Windows.Visibility.Visible
        Sports1.Visibility = Windows.Visibility.Visible
        CampusBuildings.Visibility = Windows.Visibility.Visible
        viewCampusButton.IsEnabled = False
    End Sub

    Private Sub Floorplan_Click(sender As Object, e As RoutedEventArgs) Handles viewFloorplanButton.Click
        If buildingCombobox.SelectedItem.Equals("Wubben Math and Science Center") And FloorCombobox.SelectedItem = "First Floor" Then
            scale.ScaleX = 1.0
            scale.ScaleY = 1.0
            GMH1.Visibility = Windows.Visibility.Hidden
            GMH2.Visibility = Windows.Visibility.Hidden
            GMH3.Visibility = Windows.Visibility.Hidden
            GMH4.Visibility = Windows.Visibility.Hidden
            Wubben1.Visibility = Windows.Visibility.Visible
            Wubben2.Visibility = Windows.Visibility.Hidden
            Wubben3.Visibility = Windows.Visibility.Hidden
            Parking1.Visibility = Windows.Visibility.Hidden
            Misc1.Visibility = Windows.Visibility.Hidden
            TextBlock2.Visibility = Windows.Visibility.Hidden
            Terrain1.Visibility = Windows.Visibility.Hidden
            Sports1.Visibility = Windows.Visibility.Hidden
            CampusBuildings.Visibility = Windows.Visibility.Hidden
            viewCampusButton.IsEnabled = True
        ElseIf buildingCombobox.SelectedItem.Equals("Wubben Math and Science Center") And FloorCombobox.SelectedItem = "Second Floor" Then
            scale.ScaleX = 1.0
            scale.ScaleY = 1.0
            GMH1.Visibility = Windows.Visibility.Hidden
            GMH2.Visibility = Windows.Visibility.Hidden
            GMH3.Visibility = Windows.Visibility.Hidden
            GMH4.Visibility = Windows.Visibility.Hidden
            Wubben2.Visibility = Windows.Visibility.Visible
            Wubben1.Visibility = Windows.Visibility.Hidden
            Wubben3.Visibility = Windows.Visibility.Hidden
            Parking1.Visibility = Windows.Visibility.Hidden
            Misc1.Visibility = Windows.Visibility.Hidden
            TextBlock2.Visibility = Windows.Visibility.Hidden
            Terrain1.Visibility = Windows.Visibility.Hidden
            Sports1.Visibility = Windows.Visibility.Hidden
            CampusBuildings.Visibility = Windows.Visibility.Hidden
            viewCampusButton.IsEnabled = True
        ElseIf buildingCombobox.SelectedItem.Equals("Grand Mesa Hall") And FloorCombobox.SelectedItem = "First Floor" Then
            scale.ScaleX = 1.0
            scale.ScaleY = 1.0
            GMH1.Visibility = Windows.Visibility.Visible
            GMH2.Visibility = Windows.Visibility.Hidden
            GMH3.Visibility = Windows.Visibility.Hidden
            GMH4.Visibility = Windows.Visibility.Hidden

            Wubben2.Visibility = Windows.Visibility.Hidden
            Wubben1.Visibility = Windows.Visibility.Hidden
            Wubben3.Visibility = Windows.Visibility.Hidden
            Parking1.Visibility = Windows.Visibility.Hidden
            Misc1.Visibility = Windows.Visibility.Hidden
            TextBlock2.Visibility = Windows.Visibility.Hidden
            Terrain1.Visibility = Windows.Visibility.Hidden
            Sports1.Visibility = Windows.Visibility.Hidden
            CampusBuildings.Visibility = Windows.Visibility.Hidden
            viewCampusButton.IsEnabled = True
        ElseIf buildingCombobox.SelectedItem.Equals("Grand Mesa Hall") And FloorCombobox.SelectedItem = "Second Floor" Then
            scale.ScaleX = 1.0
            scale.ScaleY = 1.0
            GMH1.Visibility = Windows.Visibility.Hidden
            GMH2.Visibility = Windows.Visibility.Visible
            GMH3.Visibility = Windows.Visibility.Hidden
            GMH4.Visibility = Windows.Visibility.Hidden
            Wubben2.Visibility = Windows.Visibility.Hidden
            Wubben1.Visibility = Windows.Visibility.Hidden
            Wubben3.Visibility = Windows.Visibility.Hidden
            Parking1.Visibility = Windows.Visibility.Hidden
            Misc1.Visibility = Windows.Visibility.Hidden
            TextBlock2.Visibility = Windows.Visibility.Hidden
            Terrain1.Visibility = Windows.Visibility.Hidden
            Sports1.Visibility = Windows.Visibility.Hidden
            CampusBuildings.Visibility = Windows.Visibility.Hidden
            viewCampusButton.IsEnabled = True
        ElseIf buildingCombobox.SelectedItem.Equals("Grand Mesa Hall") And FloorCombobox.SelectedItem = "Third Floor" Then
            scale.ScaleX = 1.0
            scale.ScaleY = 1.0
            GMH1.Visibility = Windows.Visibility.Hidden
            GMH2.Visibility = Windows.Visibility.Hidden
            GMH3.Visibility = Windows.Visibility.Visible
            GMH4.Visibility = Windows.Visibility.Hidden
            Wubben2.Visibility = Windows.Visibility.Hidden
            Wubben1.Visibility = Windows.Visibility.Hidden
            Wubben3.Visibility = Windows.Visibility.Hidden
            Parking1.Visibility = Windows.Visibility.Hidden
            Misc1.Visibility = Windows.Visibility.Hidden
            TextBlock2.Visibility = Windows.Visibility.Hidden
            Terrain1.Visibility = Windows.Visibility.Hidden
            Sports1.Visibility = Windows.Visibility.Hidden
            CampusBuildings.Visibility = Windows.Visibility.Hidden
            viewCampusButton.IsEnabled = True
        ElseIf buildingCombobox.SelectedItem.Equals("Grand Mesa Hall") And FloorCombobox.SelectedItem = "Fourth Floor" Then
            scale.ScaleX = 1.0
            scale.ScaleY = 1.0
            GMH1.Visibility = Windows.Visibility.Hidden
            GMH2.Visibility = Windows.Visibility.Hidden
            GMH3.Visibility = Windows.Visibility.Hidden
            GMH4.Visibility = Windows.Visibility.Visible
            Wubben2.Visibility = Windows.Visibility.Hidden
            Wubben1.Visibility = Windows.Visibility.Hidden
            Wubben3.Visibility = Windows.Visibility.Hidden
            Parking1.Visibility = Windows.Visibility.Hidden
            Misc1.Visibility = Windows.Visibility.Hidden
            TextBlock2.Visibility = Windows.Visibility.Hidden
            Terrain1.Visibility = Windows.Visibility.Hidden
            Sports1.Visibility = Windows.Visibility.Hidden
            CampusBuildings.Visibility = Windows.Visibility.Hidden
            viewCampusButton.IsEnabled = True
        ElseIf buildingCombobox.SelectedItem.Equals("Wubben Math and Science Center") And FloorCombobox.SelectedItem = "Third Floor" Then
            scale.ScaleX = 1.0
            scale.ScaleY = 1.0
            GMH1.Visibility = Windows.Visibility.Hidden
            GMH2.Visibility = Windows.Visibility.Hidden
            GMH3.Visibility = Windows.Visibility.Hidden
            GMH4.Visibility = Windows.Visibility.Hidden
            Wubben3.Visibility = Windows.Visibility.Visible
            Wubben1.Visibility = Windows.Visibility.Hidden
            Wubben2.Visibility = Windows.Visibility.Hidden
            Parking1.Visibility = Windows.Visibility.Hidden
            Misc1.Visibility = Windows.Visibility.Hidden
            TextBlock2.Visibility = Windows.Visibility.Hidden
            Terrain1.Visibility = Windows.Visibility.Hidden
            Sports1.Visibility = Windows.Visibility.Hidden
            CampusBuildings.Visibility = Windows.Visibility.Hidden
            viewCampusButton.IsEnabled = True
        End If


    End Sub

    Private Sub searchButton_Click(sender As Object, e As RoutedEventArgs) Handles searchButton.Click

        If searchBox.Text <> "" Then
            buildingCombobox.Items.Clear()
            For Each Building In buildings
                Dim input As String = searchBox.Text
                Dim buildingname As String = Building.buildingName
                If buildingname.ToLower().Contains(input.ToLower()) Then
                    buildingCombobox.Items.Add(buildingname)

                End If
            Next
        End If
    End Sub

    Private Sub scrlViewMap_Click(sender As Object, e As RoutedEventArgs) Handles ScrlViewMap.PreviewMouseLeftButtonDown
        p = Mouse.GetPosition(campusmap)
        mouseisDown = True
    End Sub

    Private Sub scrlViewMap_dClick(sender As Object, e As RoutedEventArgs) Handles ScrlViewMap.MouseMove
        If mouseisDown = True Then
            xchange = p.X - Mouse.GetPosition(campusmap).X
            ychange = p.Y - Mouse.GetPosition(campusmap).Y
            scale.CenterX += xchange
            scale.CenterY += ychange
            p = Mouse.GetPosition(campusmap)

        End If
    End Sub

    Private Sub scrlViewMap_lClick(sender As Object, e As RoutedEventArgs) Handles ScrlViewMap.MouseLeave
        mouseisDown = False
    End Sub

    Private Sub scrlViewMap_uClick(sender As Object, e As RoutedEventArgs) Handles ScrlViewMap.MouseUp
        mouseisDown = False
    End Sub

    Private Sub scrlViewMap_dlClick(ByVal sender As Object, ByVal e As MouseButtonEventArgs) Handles ScrlViewMap.PreviewMouseLeftButtonDown
        If e.ClickCount = 2 Then
            xchange = Mouse.GetPosition(campusmap).X
            ychange = Mouse.GetPosition(campusmap).Y
            scale.CenterY = ychange
            scale.CenterX = xchange
            ZoomIn()
        End If
    End Sub

    Private Sub scrlViewMap_drClick(ByVal sender As Object, ByVal e As MouseButtonEventArgs) Handles ScrlViewMap.MouseRightButtonDown
        If e.ClickCount = 2 Then
            xchange = Mouse.GetPosition(campusmap).X
            ychange = Mouse.GetPosition(campusmap).Y
            scale.CenterY = ychange
            scale.CenterX = xchange
            ZoomOut()
        End If
    End Sub

    Private Sub searchBox_textChanged(sender As Object, e As RoutedEventArgs) Handles searchBox.KeyUp
        searchBox.IsDropDownOpen = True
        Dim search = searchBox.Text

        While searchBox.Items.Count > 0
            searchBox.Items.RemoveAt(0)
        End While

        For Each Building In buildings
            If Building.buildingName.ToLower().Contains(search.ToLower) Then
                searchBox.Items.Add(Building.buildingName)
            End If
        Next

    End Sub

    Private Sub floorCombobox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles FloorCombobox.SelectionChanged
        viewFloorplanButton.IsEnabled = True
        RoomCombobox.IsEnabled = True
    End Sub


    Private Sub buildingCombobox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles buildingCombobox.SelectionChanged
        FloorCombobox.Items.Clear()
        If buildingCombobox.SelectedItem = "Grand Mesa Hall" Then
            FloorCombobox.Items.Add("First Floor")
            FloorCombobox.Items.Add("Second Floor")
            FloorCombobox.Items.Add("Third Floor")
            FloorCombobox.Items.Add("Fourth Floor")
        ElseIf buildingCombobox.SelectedItem = "Wubben Math and Science Center" Then
            FloorCombobox.Items.Add("First Floor")
            FloorCombobox.Items.Add("Second Floor")
            FloorCombobox.Items.Add("Third Floor")
        End If
        If FloorCombobox.SelectedItem = "" Then
            viewFloorplanButton.IsEnabled = False
        End If
        FloorCombobox.IsEnabled = True
        FloorCombobox.SelectedIndex = 0
        prevBuilding = selectedBuilding
        selectedBuilding = buildingCombobox.SelectedItem.Replace(" ", "")

        If selectedBuilding = "TomlinsonLibrary" Then
            selectedBuilding = "TomlinsonOld"
        End If
        Try
            Dim thisStoryBoard As Storyboard = DirectCast(FindResource("BuildingMouseEnter"), Storyboard)
            thisStoryBoard.SetValue(Storyboard.TargetNameProperty, TryCast(FindName(selectedBuilding), Path).Name)
            thisStoryBoard.Begin()
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
        Try
            Dim thisStoryBoard As Storyboard = DirectCast(FindResource("BuildingMouseLeave"), Storyboard)
            thisStoryBoard.SetValue(Storyboard.TargetNameProperty, TryCast(FindName(prevBuilding), Path).Name)
            thisStoryBoard.Begin()
        Catch ex As Exception
            Console.WriteLine("Building Mouseover: " & ex.Message)
        End Try
    End Sub

    Private Sub ComboBox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs)
        searchSelection = searchBox.SelectedItem
        For Each blding In buildingNames
            If blding = searchSelection Then
                buildingCombobox.SelectedItem = searchSelection
            End If
        Next
    End Sub

    Private Sub print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles printButton.Click
        'PrintDialog1.Document = PrintDocument1
        'PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        'PrintDialog1.AllowSomePages = True
        'If PrintDialog1.ShowDialog = DialogResult.OK Then
        '    PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
        '    PrintDocument1.Print()
        Dim pDialog As New PrintDialog()
        pDialog.PageRangeSelection = PageRangeSelection.AllPages
        pDialog.UserPageRangeEnabled = True

        ' Display the dialog. This returns true if the user presses the Print button.
        Dim print? As Boolean = pDialog.ShowDialog()
        'If print = True Then
        '    Dim xpsDocument As New XpsDocument("C:\FixedDocumentSequence.xps", FileAccess.ReadWrite)
        '    Dim fixedDocSeq As FixedDocumentSequence = xpsDocument.GetFixedDocumentSequence()
        '    pDialog.PrintDocument(fixedDocSeq.DocumentPaginator, "Test print job")
        'End If
    End Sub

End Class
